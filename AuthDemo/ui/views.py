from django.shortcuts import render, HttpResponse
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def signup_page(request):
    return render(request, 'signup.html', {'signup_form': UserCreationForm()})

def new_user(request):
    frm = UserCreationForm(request.POST)
    if frm.is_valid():
        frm.save()
        return HttpResponse("Created")
    return HttpResponse("Signup Failed")